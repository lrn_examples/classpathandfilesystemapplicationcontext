package com.my.pkg1;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@WebListener
public class MyWebListenerExample implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		String metadata = "mybean.xml";  // !!!
		ApplicationContext context = new ClassPathXmlApplicationContext(metadata);
		Student student = context.getBean("student", Student.class);
		System.out.println(student.age);
		System.out.println(student.name);

		String metadata2 = "my_bean_definitions/mybean2.xml";  // !!!
		ApplicationContext context2 = new ClassPathXmlApplicationContext(metadata2);
		Student student2 = context2.getBean("student", Student.class);
		System.out.println(student2.age);
		System.out.println(student2.name);
		
		//leading slash is ignored = same as above!
		String metadata3 = "/my_bean_definitions/mybean2.xml";   // !!!
		ApplicationContext context3 = new ClassPathXmlApplicationContext(metadata3);
		Student student3 = context3.getBean("student", Student.class);
		System.out.println(student3.age);
		System.out.println(student3.name);

	}

}
