/*
 * 
 * 1) ClassPathXmlApplicationContext (for WAR - to be used within app server) 

   2) FileSystemXmlApplicationContext (stand-alone app with main, for testing)
   
   bean definition xml file is in some resource folder (create yourself, make it a source folder) 
   or its subfolder (subfolder shall NOT be a source folder!)
   
   P.S. Project Properties -> Targeted Runtime -> Tomcat9 (yellow field shall be filled!)

*/


package com.my.pkg1;