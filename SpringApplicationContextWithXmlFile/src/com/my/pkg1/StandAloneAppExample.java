package com.my.pkg1;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class StandAloneAppExample {

	public static void main(String[] args) {

		// "file://D:\\mybean.xml"; ERR !!!

		// 1-5 are fine!
		ApplicationContext context1 = new FileSystemXmlApplicationContext("D:\\dir1\\mybean.xml");
		ApplicationContext context2 = new FileSystemXmlApplicationContext("D:/dir1/mybean.xml");

		ApplicationContext context2_1 = new FileSystemXmlApplicationContext("D:\\dir1\\mybean.xml");
		ApplicationContext context2_2 = new FileSystemXmlApplicationContext("/D:\\dir1\\mybean.xml");

		// and "file: + all-4-variants-above"
		ApplicationContext context3 = new FileSystemXmlApplicationContext("file:D:\\dir1\\mybean.xml");

		// and "file:/ + all-4-variants-above"
		ApplicationContext context4 = new FileSystemXmlApplicationContext("file:/D:/dir1\\mybean.xml");

		ApplicationContext context5 = new FileSystemXmlApplicationContext("file:\\D:/dir1\\mybean.xml");

		// c.ERR
		// ApplicationContext context6 = new
		// FileSystemXmlApplicationContext("file://D:/dir1\\mybean.xml");

		Student student = context5.getBean("student", Student.class);
		System.out.println(student.age);
		System.out.println(student.name);

		Path p1 = Paths.get("D:\\dir1\\mybean.xml");
		System.out.println(p1);

		Path p2 = Paths.get("D:/dir1/mybean.xml");
		System.out.println(p2);

		Path p3 = Paths.get("D:\\dir1\\mybean.xml");
		System.out.println(p3);

		Path p4 = Paths.get("D:/dir1/mybean.xml");
		System.out.println(p4);

		// evth w/ file: is InvalidPathException
//		Path p5 = Paths.get("D:\\dir1\\mybean.xml"); // InvalidPathException
//		System.out.println(p5);
//		
//		Path p6 = Paths.get("file:/dir1\\mybean.xml"); // InvalidPathException
//		System.out.println(p6);
//		
//		Path p7 = Paths.get("file:\\D:/dir1\\mybean.xml"); // InvalidPathException
//		System.out.println(p7);

//		Path p8 = Paths.get("file://D:/dir1\\mybean.xml"); // InvalidPathException
//		System.out.println(p8);		

	}

}
